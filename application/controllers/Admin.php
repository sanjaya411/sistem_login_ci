<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('status') != TRUE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal masuk!","Gagal masuk! Anda wajib login terlebih dahulu!","error","tutup"); </script>');
      redirect('login');
    }
  }

  public function index()
  {
    $data = [
      'title' => 'ADMIN',
      'nama' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'user')->row()
    ];
    $this->load->view('admin/index',$data);
  }
}