<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email',[
			'valid_email' => 'Masukan format email yang benar!',
			'required' => 'Wajib masukan email!'
		]);
		$this->form_validation->set_rules('password', 'password', 'required|min_length[4]',[
			'required' => 'Wajib masukan password!',
			'min_length' => 'Masukan minimal 4 karakter!'
		]);
		
		
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Login'
			];
			$this->load->view('header',$data);
			$this->load->view('login',$data);
			$this->load->view('footer');
		} else {
			$this->prosesLogin();
		}
	}

	private function prosesLogin()
	{
		$email 					= html_escape($this->input->post('email',true));
		$pass 					= html_escape($this->input->post('password',true));

		$cek = $this->M_data->editData(['user_email' => $email],'user')->row();

		if($cek) {
			if(password_verify($pass,$cek->user_password)) {
				$data = [
					'id' => $cek->user_id,
					'status' => True
				];
				$this->session->set_userdata($data);
				redirect('admin');
			} else {
				$this->session->set_flashdata('pesan', '<script>sweet("Gagal login!","Gagal login! Password yang anda masukan salah!","error","tutup"); </script>');
				redirect('login');
			}
		} else {
			$this->session->set_flashdata('pesan', '<script>sweet("Gagal login!","Gagal login! Akun anda belum terdaftar!","error","tutup"); </script>');
			redirect('login');
		}
	}

	public function lupaPassword()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email',[
			'required' => 'Wajib masukan email!',
			'valid_email' => 'Masukan format email yang benar!'
		]);
		
		
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Reset password'
			];
			$this->load->view('header',$data);
			$this->load->view('verif_email',$data);
			$this->load->view('footer');
		} else {
			$email = html_escape($this->input->post('email',true));
			$token = rand(10000,100000);

			$this->load->library('email');

			$this->email->from('sanjaya@localhost', 'Mohammad Ricky Sanjaya');
			$this->email->to($email);
			$this->email->subject('Token reset password');
			$this->email->message('Token reset password anda adalah '.$token);
			$this->email->send();

			$data = [
				'email' => $email,
				'token' => $token
			];			
			$this->session->set_userdata( $data );		

			redirect('verifToken');			
		}
	}

	public function verifToken()
	{
		if(!$this->session->userdata('email')) {
			$this->session->set_flashdata('pesan', '<script>sweet("Gagal masuk!","Gagal masuk! Anda wajib login terlebih dahulu!","error","tutup"); </script>');
			redirect('login');
		} else {
			$this->form_validation->set_rules('token', 'Token', 'required|min_length[4]',[
				'required' => 'Wajib masukan nomor reset password!',
				'min_length' => 'Karakter minimal 4!'
			]);
			
			
			if ($this->form_validation->run() == FALSE) {
				$data = [
					'title' => 'Reset password'
				];
				$this->load->view('header',$data);
				$this->load->view('verif_token',$data);
				$this->load->view('footer');
			} elseif($this->session->userdata('token') != $this->input->post('token')) {
					$this->session->set_flashdata('pesan', '<script>sweet("Gagal masuk!","Gagal masuk! Nomor token anda salah!","error","tutup"); </script>');
					redirect('verifToken');
			} else {
				$data = ['status' => 'reset'];
				$this->session->set_userdata($data);
				redirect('resetPassword');
			}
		}		
	}

	public function resetPassword()
	{
		if(!$this->session->userdata('email')) {
			$this->session->set_flashdata('pesan', '<script>sweet("Gagal masuk!","Gagal masuk! Anda wajib login terlebih dahulu!","error","tutup"); </script>');
			redirect('login');			
		} else {
			if(!$this->session->userdata('status')) {
				$this->session->set_flashdata('pesan', '<script>sweet("Gagal masuk!","Gagal masuk! Anda wajib login terlebih dahulu!","error","tutup"); </script>');
				redirect('login');
			} else {
				$this->form_validation->set_rules('pass', 'Password', 'trim|required|min_length[4]|matches[pass1]',[
					'required' => 'Wajib masukan password!',
					'trim' => 'Password tidak boleh ada spasi!',
					'min_length' => 'Minimal 4 karakter!',
					'matches' => 'Password tidak sesuai'
				]);
				$this->form_validation->set_rules('pass1', 'Password', 'trim|required|min_length[4]|matches[pass]',[
					'required' => 'Wajib masukan password!',
					'trim' => 'Password tidak boleh ada spasi!',
					'min_length' => 'Minimal 4 karakter!',
					'matches' => 'Password tidak sesuai'
				]);			
				
				
				if ($this->form_validation->run() == FALSE) {
					$data = [
						'title' => 'Reset password'
					];
					$this->load->view('header',$data);
					$this->load->view('reset_password',$data);
					$this->load->view('footer');
				} else {
					$email 			= $this->session->userdata('email');
					$pass				= html_escape($this->input->post('pass',true));
	
					$data = ['user_password' => password_hash($pass,PASSWORD_DEFAULT)];
					$where = ['user_email' => $email];
					$this->M_data->updateData($data,$where,'user');
					$this->session->set_flashdata('pesan', '<script>sweet("Sukses reset password!","Password anda berhasil direset! Silahkan login!","success","tutup"); </script>');
					redirect('login');
				}
			}			
		}		
	}

	public function register()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required',[
			'required' => 'Wajib masukan nama anda!'
		]);
		$this->form_validation->set_rules('hp', 'Nomor HP', 'required|numeric',[
			'required' => 'Wajib masukan nomor HP anda!',
			'numeric' => 'Masukan nomor HP anda dengan benar!'
		]);
		$this->form_validation->set_rules('email', 'Email', 'required|is_unique[user.user_email]|valid_email',[
			'required' => 'Wajib masukan email anda!',
			'is_unique' => 'Email sudah pernah didaftarkan!',
			'valid_email' => 'Masukan format email yang benar!'
		]);
		$this->form_validation->set_rules('pass', 'pass', 'required|trim|matches[pass1]|min_length[4]',[
			'required' => 'Wajib masukan password anda!',
			'trim' => 'Dilarang menggunakan spasi untuk password!',
			'matches' => 'Password tidak sesuai!',
			'min_length' => 'Masukan minimal 4 karakter!'
		]);
		$this->form_validation->set_rules('pass1', 'password', 'required|trim|matches[pass]|min_length[4]',[
			'required' => 'Wajib masukan password anda!',
			'trim' => 'Dilarang menggunakan spasi untuk password!',
			'matches' => 'Password tidak sesuai!',
			'min_length' => 'Masukan minimal 4 karakter!'
		]);

		
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Register'
			];
			$this->load->view('header',$data);
			$this->load->view('register',$data);
			$this->load->view('footer');
		} else {
			$this->prosesRegis();
		}	
	}

	private function prosesRegis()
	{
		$nama 			= html_escape($this->input->post('nama',true));
		$email 			= html_escape($this->input->post('email',true));
		$noHp 			= html_escape($this->input->post('hp',true));
		$pass 			= password_hash($this->input->post('pass',true), PASSWORD_DEFAULT);
		$token			= rand(10000, 100000);

		
		$array = array(
			'nama' => $this->db->escape_str($nama),
			'email' => $this->db->escape_str($email),
			'hp' => $this->db->escape_str($noHp),
			'pass' => $pass,
			'token' => $token
		);

		$this->load->library('email');

		$this->email->from('sanjaya@localhost', 'Mohammad Ricky Sanjaya');
		$this->email->to($email);
		$this->email->subject('Token verifikasi registrasi');
		$this->email->message('Terima kasih sudah mendaftar, nomor pendaftaran anda adalah '.$token);
		$this->email->send();
		
		$this->session->set_userdata($array);
		redirect('verifikasi');
	}

	public function verifikasi()
	{
		if(!$this->session->userdata('token')) {
			$this->session->set_flashdata('pesan', '<script>sweet("Gagal masuk!","Gagal masuk! Anda wajib registrasi terlebih dahulu!","error","tutup"); </script>');
			redirect('register');
		} else {
			$this->form_validation->set_rules('verif', 'verifikasi', 'required|min_length[4]',[
				'required' => 'Wajib masukan nomor pendaftaran!',
				'min_length' => 'Nomor pendaftaran minimal 4 nomor!'
			]);
			
			
			if ($this->form_validation->run() == FALSE) {
				$data = [
					'title' => 'Verifikasi Pendaftaran'
				];
				$this->load->view('header',$data);
				$this->load->view('verifikasi',$data);
				$this->load->view('footer');
			} elseif($this->session->userdata('token') != $this->input->post('verif',true)) {
				$this->session->set_flashdata('pesan', '<script>sweet("Gagal regsitrasi!","Gagal registrasi! Nomor registrasi anda salah!","error","tutup"); </script>');
				redirect('verifikasi');
			} else {
				$this->prosesRegisAct();
			}
		}		
	}

	private function prosesRegisAct()
	{
		$data = [
			'user_nama' => $this->session->userdata('nama'),			
			'user_noHp' => $this->session->userdata('hp'),			
			'user_email' => $this->session->userdata('email'),			
			'user_password' => $this->session->userdata('pass')		
		];

		$this->M_data->insertData($data,'user');
		$this->session->set_flashdata('pesan', '<script>sweet("Sukses regsitrasi!","Sukses registrasi! Silahkan login!","success","tutup"); </script>');
		redirect('login');
	}

	public function logout()
	{
		$this->session->set_flashdata('pesan', '<script>sweet("Sukses logout!","Anda telah logout!","success","tutup"); </script>');
		redirect('login');
	}
}
