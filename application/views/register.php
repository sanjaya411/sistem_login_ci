<?= $this->session->flashdata('pesan'); 
  echo $this->session->sess_destroy();  
?>
<div class="container">
  <div class="row">
    <div class="col-md-8 mx-auto">
      <div class="card mt-5">
        <div class="card-header">
          <h4>Register</h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-10 mx-auto">
              <?= form_open('register'); ?>
                <div class="form-group">
                  <label>Nama Anda</label>
                  <input type="text" name="nama" class="form-control" placeholder="Masukan nama anda" value="<?= set_value('nama'); ?>" required>
                  <?= form_error('nama','<small class="text-danger">','</small>'); ?>
                </div>
                <div class="form-group">
                  <label>Nomor HP</label>
                  <input type="number" name="hp" class="form-control" placeholder="Masukan nomor handphone anda" value="<?= set_value('hp'); ?>" required>
                  <?= form_error('hp','<small class="text-danger">','</small>'); ?>
                </div>
                <div class="form-group">
                  <label>Email Anda</label>
                  <input type="email" name="email" class="form-control" placeholder="Masukan email anda" value="<?= set_value('email'); ?>" required>
                  <?= form_error('email','<small class="text-danger">','</small>'); ?>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Password Baru</label>
                      <input type="password" name="pass" class="form-control" placeholder="Masukan Password anda" required>
                      <?= form_error('pass','<small class="text-danger">','</small>'); ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Masukan Password Kembali</label>
                      <input type="password" name="pass1" class="form-control" placeholder="Masukan Ulang Password anda" required>
                      <?= form_error('pass1','<small class="text-danger">','</small>'); ?>
                    </div>
                  </div>
                </div>
                <input type="submit" value="Register" class="btn btn-success">
              <?= form_close(); ?>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <p>Sudah punya akun? <a href="<?= base_url('login'); ?>">Login</a></p>
        </div>
      </div>
    </div>
  </div>
</div>