<?= $this->session->flashdata('pesan'); ?>
<div class="container">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card mt-5">
        <div class="card-body">
          <div class="alert alert-info">Nomor pendaftaran telah dikirim ke alamat email anda</div>
          <?= form_open('verifikasi'); ?>
            <div class="form-group">
              <label>Masukan nomor verifikasi</label>
              <input type="number" name="verif" class="form-control" required>
              <?= form_error('verif','<small class="text-danger">','</small>'); ?>
            </div>
            <input type="submit" value="Register" class="btn btn-success">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>