<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">	

	<title><?= $title; ?></title>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?= base_url('asset/css/bootstrap.min.css'); ?>">

	<!-- Sweet Alert -->
	<script src="<?= base_url('asset/js/sweet.js'); ?>"></script>
	<script src="<?= base_url('asset/js/custom.js'); ?>"></script>

</head>

<body class="bg-primary">