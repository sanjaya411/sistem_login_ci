<?= $this->session->flashdata('pesan'); 
  echo $this->session->sess_destroy();  
?>
	<div class="container">
		<div class="row">
			<div class="col-md-5 mx-auto">
				<div class="card mt-5">
					<div class="card-header">
						<h4>Login</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-10 mx-auto">
								<?= form_open('login'); ?>
								<?= form_error('email','<small class="text-danger">','</small>'); ?>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-envelope"></i></span>
										</div>
										<input type="email" name="email" class="form-control" placeholder="Masukan Email" value="<?= set_value('email'); ?>" required>
									</div>
									<?= form_error('password','<small class="text-danger">','</small>'); ?>
									<div class="input-group mb-3">										
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-key"></i></span>
										</div>
										<input type="password" name="password" class="form-control" placeholder="Masukan Password" required>
									</div>									
									<div class="row">
										<div class="col-4">
											<input type="submit" value="Login" class="btn btn-success btn-sm">
										</div>
										<div class="col-8 text-right">
											<a href="<?= base_url('lupaPassword'); ?>" class="text-decoration-none">Lupa Password?</a>
										</div>
									</div>
								<?= form_close(); ?>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<p>Belum mempunyai akun? <a href="<?= base_url('register'); ?>">Register sekarang!</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>