<?= $this->session->flashdata('pesan'); ?>
<div class="container">
  <div class="row mt-5">
    <div class="col-md-6 mx-auto">
      <div class="card">
        <div class="card-body">
          <div class="alert alert-info">Nomor token reset akun sudah terkirim ke email anda!</div>
          <?= form_open('verifToken'); ?>
            <div class="form-group">
              <label>Token Reset</label>
              <input type="number" name="token" class="form-control" required>
              <?= form_error('token','<small class="text-danger">','</small>'); ?>
            </div>
            <input type="submit" value="Verifikasi" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>